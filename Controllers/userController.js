const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Course = require("../Models/coursesSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



// Controllers


//This controller will create or register a user on our db/database
module.exports.userRegistration = (request, response) => {

		const input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send("The email is already taken!")
			}else{
				let newUser = new User({
					firstName: input.firstName,
					lastName: input.lastName,
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					mobileNo: input.mobileNo
				})


				//save to database
				newUser.save()
				.then(save => {
					return response.send("You are now registered to our website!")
				})
				.catch(error => {
					return response.send(error)
				})





			}

		})
		.catch(error => {
			return response.send(error)
		})
}

//User Authentication
module.exports.userAuthentication = (request, response) => {
		let input = request.body;

		//Possible scenarios in logging in
			//1. email is not yet registered.
			//2. email is registered but the password is wrong

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return response.send("Email is not yet registered. Register first before logging in!")
			}else{
				// we have to verify if the passwor is correct
				// The "compareSync" method is used to compare a non encrypted password to the encrypted password.
				//it returns boolean value, if match true value will return otherwise false.
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)});
				}else{
					return response.send("Password is incorrect!")
				}

			}




		})
		.catch(error => {
			return response.send(error);
		})

}

// Retrieve the user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the result document to an empty string("").
	3. Return the result back to the frontend
*/

module.exports.getProfile = (request, response) => {
	// let input = request.body;
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	return User.findById(userData._id).then(result =>{
		// avoid to expose sensitive information such as password.
		result.password = "";

		return response.send(result);
	})
}

// controller for user enrollment:
	// 1. we can get the id of the user by decoding the jwt
	// 2. we can get the courseId by using the request params

module.exports.enrollCourse = async (request,response) => {
	// first we have to get the userId and the courseId
	const userData = auth.decode(request.headers.authorization);

	// get the courseId by tergetting the params in the url
	const courseId = request.params.courseId;
	let user = userData._id;

	// 2 things that we need to do in this controller
		// first, to push the courseId in the enrollments property of the user
		// second, to push the userId in the enrollees property of the course.
	if(userData.isAdmin) {
		return response.send("Admin is not allowed for enrollment.")
	}
	else{
		let foundCourseId = await Course.findById(courseId)
		.then(result=>true)
		.catch(result=>false)

		if(!foundCourseId) {

			return response.send("There was an error during the enrollment. Please try again!")
		}

		else {

			await User.findById(user)
			.then(result => {
				if(result === null) {
					return false
				}else{
					result.enrollments.push({courseId: courseId})
					return result.save()
					.then(save => result)
					.catch(error => false)
				}
			})
			.catch(error => response.send(error))

			await Course.findById(courseId)
			.then(result => {
				if(result === null) {
					return false;
				}
				else {
					result.enrollees.push({userId:user})
					return result.save()
					.then(result => true)
					.catch(error => false);
				}
			})

			return response.send("The course is now enrolled.")

		}

	}

}